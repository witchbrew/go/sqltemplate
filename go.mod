module gitlab.com/witchbrew/go/sqltemplate

go 1.14

require (
	github.com/stretchr/testify v1.6.1
	github.com/valyala/fasttemplate v1.2.0
)
