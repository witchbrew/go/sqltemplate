package sqltemplate

import (
	"github.com/valyala/fasttemplate"
	"regexp"
	"strings"
)

type Template struct {
	t *fasttemplate.Template
}

type Params = map[string]interface{}

var spacesRegexp = regexp.MustCompile(`\s+`)

func New(template string) *Template {
	template = spacesRegexp.ReplaceAllString(template, " ")
	return &Template{
		t: fasttemplate.New(strings.TrimSpace(template), "{{", "}}"),
	}
}

func (t *Template) Render(params Params) string {
	return t.t.ExecuteString(params)
}
