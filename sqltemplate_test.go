package sqltemplate_test

import (
	"github.com/stretchr/testify/require"
	"gitlab.com/witchbrew/go/sqltemplate"
	"testing"
)

var simpleT = sqltemplate.New("hello")

func TestSimple(t *testing.T) {
	require.Equal(t, "hello", simpleT.Render(sqltemplate.Params{}))
}

var leadingTrailingWhiteSpacesT = sqltemplate.New(`
hello
`)

func TestLeadingTrailingWhiteSpace(t *testing.T) {
	require.Equal(t, "hello", leadingTrailingWhiteSpacesT.Render(sqltemplate.Params{}))
}

var tabsT = sqltemplate.New(`
	one
	two
	{{param}}
`)

func TestTabs(t *testing.T) {
	expected := "one two three"
	require.Equal(t, expected, tabsT.Render(sqltemplate.Params{
		"param": "three",
	}))
}
